/*
 * VIC-SM8 CPU by matthilde
 *
 * cpu.h
 *
 * CPU headers
 */
#ifndef _VICSM8_CPU
#define _VICSM8_CPU

#include <stdint.h>

// Memory size
#define _VICSM8_MEMORY  0x10000
// Stack size
#define _VICSM8_STACKSZ 512

// Flags
#define _VICSM8_HF      0x01    // Halt flag
#define _VICSM8_ZF      0x02    // Zero flag
#define _VICSM8_CF      0x04    // Carry flag
#define _VICSM8_SF      0x08    // Sign flag
#define _VICSM8_TF      0x10    // Trap flag

// types
typedef uint8_t         byte;
typedef uint16_t        word;
typedef int8_t          sbyte;
typedef int16_t         sword;

// Stack
typedef struct
{
    word        ptr;
    byte        s[_VICSM8_STACKSZ];
} vcpu_stack_t;

typedef struct _vicsm8_cpu vcpu_t;

// IO Handler
typedef void            (*vcpu_io_t)(vcpu_t*, byte);

// IO Port
typedef struct
{
    int         enabled;    // is the device enabled?
    char        name[8];    // Short name for the device

    vcpu_io_t   start;      // Startup handler
    vcpu_io_t   in;         // IN handler
    vcpu_io_t   out;        // OUT handler
    vcpu_io_t   stopl;      // Stop handler
} vcpu_ioport_t;

// CPU struct
struct _vicsm8_cpu
{
    // Memory
    byte            ram[_VICSM8_MEMORY];
    // Stacks and registers
    vcpu_stack_t    ds;         // Data stack
    vcpu_stack_t    rs;         // Return stack
    word            pc;         // Program counter
    word            ip;         // Interrupt pointer
    word            ar;         // Address Register

    // Flags
    byte            flags;

    // I/O Ports
    vcpu_ioport_t   ports[256];
};


// Prototypes
void init_cpu(vcpu_t* cpu);
void exeopcode(vcpu_t* cpu, byte opcode);
void execute(vcpu_t* cpu);

void spush(vcpu_stack_t*, byte);
byte spop(vcpu_stack_t*);
void spush16(vcpu_stack_t*, word);
word spop16(vcpu_stack_t*);
void interrupt_call(vcpu_t*, byte);
#endif
