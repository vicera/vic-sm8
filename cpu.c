/*
 * VIC-SM8 CPU by matthilde
 *
 * cpu.c
 *
 * This file contains the whole CPU emulator.
 */
#include <stdio.h>

#include "cpu.h"

// Flag operations
#define SETFLAG(x)      cpu->flags |= x
#define RESETFLAG(x)    cpu->flags &= ~(x)
#define LITEM(x)        (x.s + x.ptr - 1)
#define DDROP(x)        SPOP(x); SPOP(x)

// Opcode function definition
typedef void (*opcode_t)(vcpu_t*);

// Stack operations
void spush(vcpu_stack_t* s, byte value)
{
    s->s[(s->ptr)++] = value;
    s->ptr %= _VICSM8_STACKSZ;
}
byte spop(vcpu_stack_t* s)
{ 
    byte r = s->s[--(s->ptr)];
    s->ptr %= _VICSM8_STACKSZ;
    
    return r;
}

// Return Stack operations

void rspush(vcpu_stack_t *s, word value)
{ 
    ((word*)s->s)[(s->ptr)++] = value;
    s->ptr %= _VICSM8_STACKSZ / 2;
}
word rspop(vcpu_stack_t *s) 
{
    word r = ((word*)s->s)[--(s->ptr)];
    s->ptr %= _VICSM8_STACKSZ / 2;
    
    return r;
}

// 16-bit Stack operations
word spop16(vcpu_stack_t* s)
{
    byte a = spop(s);
    return (spop(s) << 8) | a;
}
void spush16(vcpu_stack_t* s, word value)
{
    spush(s, value >> 8);
    spush(s, value);
}

// Pushes a specific amount of bytes in the stack
void push_nums(vcpu_stack_t* s, byte amount, byte* nums)
{
    for (byte i = 0; i < amount; ++i)
        spush(s, nums[i]);
}

typedef void (*vcpu_opcode_t)(vcpu_t*);

// Bytecode functions

// 8-bit opcodes
void o_halt(vcpu_t* cpu)    { SETFLAG(_VICSM8_HF); }
void o_num(vcpu_t* cpu, byte amount)
{
    // spush(&cpu->ds, cpu->ram[++cpu->pc]);
    byte *nums  = cpu->ram + cpu->pc + 1;
    
    push_nums(&cpu->ds, amount, nums);
    cpu->pc += amount;
}
void o_int(vcpu_t* cpu)     { interrupt_call(cpu, spop(&cpu->ds)); }
void o_in(vcpu_t* cpu) 
{
    int a = spop(&cpu->ds);
    cpu->ports[a].in(cpu, a);
}
void o_out(vcpu_t* cpu)
{
    int a = spop(&cpu->ds);
    cpu->ports[a].out(cpu, a);
}

void o_rot(vcpu_t* cpu)
{
    byte a = spop(&cpu->ds), b = spop(&cpu->ds), c = spop(&cpu->ds);
    spush(&cpu->ds, b); spush(&cpu->ds, a); spush(&cpu->ds, c);
}
void o_swp(vcpu_t* cpu)
{
    byte a = spop(&cpu->ds), b = spop(&cpu->ds);
    spush(&cpu->ds, a); spush(&cpu->ds, b);
}
void o_dup(vcpu_t* cpu)     { spush(&cpu->ds, *LITEM(cpu->ds)); }
void o_over(vcpu_t* cpu)    { spush(&cpu->ds, *(LITEM(cpu->ds)-1)); }
void o_drop(vcpu_t* cpu)    { spop(&cpu->ds); }
void o_add(vcpu_t* cpu)     { spush(&cpu->ds, spop(&cpu->ds) + spop(&cpu->ds)); }
void o_sub(vcpu_t* cpu)
{
    byte a = spop(&cpu->ds);
    spush(&cpu->ds, spop(&cpu->ds) - a);
}
void o_mul(vcpu_t* cpu)     { spush(&cpu->ds, spop(&cpu->ds) * spop(&cpu->ds)); }
void o_div(vcpu_t* cpu)
{
    byte a = spop(&cpu->ds);
    spush(&cpu->ds, spop(&cpu->ds) / a);
}
void o_mod(vcpu_t* cpu)
{
    byte a = spop(&cpu->ds);
    spush(&cpu->ds, spop(&cpu->ds) % a);
}

void o_and(vcpu_t* cpu)     { spush(&cpu->ds, spop(&cpu->ds) & spop(&cpu->ds)); }
void o_or(vcpu_t* cpu)      { spush(&cpu->ds, spop(&cpu->ds) | spop(&cpu->ds)); }
void o_not(vcpu_t* cpu)     { spush(&cpu->ds, ~(spop(&cpu->ds))); }
void o_xor(vcpu_t* cpu)     { spush(&cpu->ds, spop(&cpu->ds) ^ spop(&cpu->ds)); }
void o_sr(vcpu_t* cpu)      { spush(&cpu->ds, spop(&cpu->ds) >> 1); }
void o_sl(vcpu_t* cpu)      { spush(&cpu->ds, spop(&cpu->ds) << 1); }

void o_inv(vcpu_t* cpu)     { spush(&cpu->ds, !spop(&cpu->ds)); }

void o_ldb(vcpu_t* cpu)     { spush(&cpu->ds, cpu->ram[spop16(&cpu->ds)]); }
void o_stb(vcpu_t* cpu)
{
    word addr = spop16(&cpu->ds);
    byte a    = spop(&cpu->ds);
    
    cpu->ram[addr] = a;
}
void o_ldw(vcpu_t* cpu)
{
    // spaghet code
    word addr = spop16(&cpu->ds);
    word* ptr = (word*)(cpu->ram + addr);

    spush16(&cpu->ds, *ptr);
}
void o_stw(vcpu_t* cpu)
{
    // More spaghet code
    word addr = spop16(&cpu->ds);
    word a    = spop16(&cpu->ds);
    word* ptr = (word*)(cpu->ram + addr);
    
    *ptr = a;
}
void o_adl(vcpu_t* cpu)     { spush(&cpu->ds, cpu->ram[cpu->ar]); }
void o_ads(vcpu_t* cpu)
{
    byte a    = spop(&cpu->ds);
    cpu->ram[cpu->ar] = a;
}

void o_equ(vcpu_t* cpu)     { spush(&cpu->ds, spop(&cpu->ds) == spop(&cpu->ds)); }
void o_neq(vcpu_t* cpu)     { spush(&cpu->ds, spop(&cpu->ds) != spop(&cpu->ds)); }
void o_grt(vcpu_t* cpu)
{
    byte a = spop(&cpu->ds);
    spush(&cpu->ds, spop(&cpu->ds) > a);
}
void o_lrt(vcpu_t* cpu)
{
    byte a = spop(&cpu->ds);
    spush(&cpu->ds, spop(&cpu->ds) < a);
}
void o_geq(vcpu_t* cpu)
{
    byte a = spop(&cpu->ds);
    spush(&cpu->ds, spop(&cpu->ds) >= a);
}
void o_leq(vcpu_t* cpu)
{
    byte a = spop(&cpu->ds);
    spush(&cpu->ds, spop(&cpu->ds) <= a);
}

void o_jmp(vcpu_t* cpu)     { cpu->pc = spop16(&cpu->ds) - 1; }
void o_jmz(vcpu_t* cpu)
{ 
    o_rot(cpu);
    if (spop(&cpu->ds))
        o_jmp(cpu);
    else
        spop16(&cpu->ds);
}
void o_cal(vcpu_t* cpu)
{
    rspush(&cpu->rs, cpu->pc);
    cpu->pc = spop16(&cpu->ds) - 1;
}
void o_caz(vcpu_t* cpu)
{
    o_rot(cpu);
    if (spop(&cpu->ds))
        o_cal(cpu);
    else
        spop16(&cpu->ds);
}
void o_ret(vcpu_t* cpu)
{
    cpu->pc = rspop(&cpu->rs);
}
void o_rez(vcpu_t* cpu)
{
    o_rot(cpu);
    if (spop(&cpu->ds))
        o_cal(cpu);
    else
        spop16(&cpu->ds);
}

void o_inc(vcpu_t* cpu)        { (*LITEM(cpu->ds))++; }
void o_dec(vcpu_t* cpu)        { (*LITEM(cpu->ds))--; }
void o_rnc(vcpu_t* cpu)        { cpu->ram[spop16(&cpu->ds)]++; }
void o_rec(vcpu_t* cpu)        { cpu->ram[spop16(&cpu->ds)]--; }

void o_adr(vcpu_t* cpu)        { spush16(&cpu->ds, cpu->ar); }
void o_adw(vcpu_t* cpu)        { cpu->ar = spop16(&cpu->ds); }
void o_adi(vcpu_t* cpu)        { ++(cpu->ar); }

void o_rpush(vcpu_t* cpu)      { rspush(&cpu->rs, spop16(&cpu->ds)); }
void o_rpop(vcpu_t* cpu)       { spush16(&cpu->ds, rspop(&cpu->rs)); }
void o_rinc(vcpu_t* cpu)       { rspush(&cpu->rs, rspop(&cpu->rs)+1); }

// Signed operations
void o_sadd(vcpu_t* cpu)
{

    spush(&cpu->ds, (sbyte)spop(&cpu->ds) + (sbyte)spop(&cpu->ds));
}
void o_ssub(vcpu_t* cpu)
{
    sbyte a = (sbyte)spop(&cpu->ds);
    spush(&cpu->ds, (sbyte)spop(&cpu->ds) - a);
}
void o_smul(vcpu_t* cpu)
{ spush(&cpu->ds, (sbyte)spop(&cpu->ds) * (sbyte)spop(&cpu->ds)); }
void o_sdiv(vcpu_t* cpu)
{
    sbyte a = (sbyte)spop(&cpu->ds);
    spush(&cpu->ds, (sbyte)spop(&cpu->ds) / a);
}
void o_sgrt(vcpu_t* cpu)
{
    sbyte a = (sbyte)spop(&cpu->ds);
    spush(&cpu->ds, (sbyte)spop(&cpu->ds) > a);
}
void o_slrt(vcpu_t* cpu)
{
    sbyte a = (sbyte)spop(&cpu->ds);
    spush(&cpu->ds, (sbyte)spop(&cpu->ds) < a);
}
void o_sgeq(vcpu_t* cpu)
{
    sbyte a = (sbyte)spop(&cpu->ds);
    spush(&cpu->ds, (sbyte)spop(&cpu->ds) >= a);
}
void o_sleq(vcpu_t* cpu)
{
    sbyte a = (sbyte)spop(&cpu->ds);
    spush(&cpu->ds, (sbyte)spop(&cpu->ds) <= a);
}


///////////////////////////////////////////////////////////////////////////////
// 16-bit
void o_rot16(vcpu_t* cpu)
{
    word a = spop16(&cpu->ds), b = spop16(&cpu->ds), c = spop16(&cpu->ds);
    spush16(&cpu->ds, b); spush16(&cpu->ds, a); spush16(&cpu->ds, c);
}
void o_swp16(vcpu_t* cpu)
{
    word a = spop16(&cpu->ds), b = spop16(&cpu->ds);
    spush16(&cpu->ds, a); spush16(&cpu->ds, b);
}
void o_dup16(vcpu_t* cpu)
{ 
    word w = spop16(&cpu->ds);
    spush16(&cpu->ds, w);
    spush16(&cpu->ds, w);
}
void o_over16(vcpu_t* cpu)
{ 
    word a = spop16(&cpu->ds);
    word b = spop16(&cpu->ds);
    spush16(&cpu->ds, b);
    spush16(&cpu->ds, a);
    spush16(&cpu->ds, b);
}
void o_drop16(vcpu_t* cpu)    { spop16(&cpu->ds); }
void o_add16(vcpu_t* cpu)     { spush16(&cpu->ds, spop16(&cpu->ds) + spop16(&cpu->ds)); }
void o_sub16(vcpu_t* cpu)
{
    word a = spop16(&cpu->ds);
    spush16(&cpu->ds, spop16(&cpu->ds) - a);
}
void o_mul16(vcpu_t* cpu)     { spush16(&cpu->ds, spop16(&cpu->ds) * spop16(&cpu->ds)); }
void o_div16(vcpu_t* cpu)
{
    word a = spop16(&cpu->ds);
    spush16(&cpu->ds, spop16(&cpu->ds) / a);
}
void o_mod16(vcpu_t* cpu)
{
    word a = spop16(&cpu->ds);
    spush16(&cpu->ds, spop16(&cpu->ds) % a);
}

void o_and16(vcpu_t* cpu)     { spush16(&cpu->ds, spop16(&cpu->ds) & spop16(&cpu->ds)); }
void o_or16(vcpu_t* cpu)      { spush16(&cpu->ds, spop16(&cpu->ds) | spop16(&cpu->ds)); }
void o_not16(vcpu_t* cpu)     { spush16(&cpu->ds, ~(spop16(&cpu->ds))); }
void o_xor16(vcpu_t* cpu)     { spush16(&cpu->ds, spop16(&cpu->ds) ^ spop16(&cpu->ds)); }
void o_sr16(vcpu_t* cpu)      { spush16(&cpu->ds, spop16(&cpu->ds) >> 1); }
void o_sl16(vcpu_t* cpu)      { spush16(&cpu->ds, spop16(&cpu->ds) << 1); }

void o_equ16(vcpu_t* cpu)     { spush(&cpu->ds, spop16(&cpu->ds) == spop16(&cpu->ds)); }
void o_neq16(vcpu_t* cpu)     { spush(&cpu->ds, spop16(&cpu->ds) != spop16(&cpu->ds)); }
void o_grt16(vcpu_t* cpu)
{
    word a = spop16(&cpu->ds);
    spush(&cpu->ds, spop16(&cpu->ds) > a);
}
void o_lrt16(vcpu_t* cpu)
{
    word a = spop16(&cpu->ds);
    spush(&cpu->ds, spop16(&cpu->ds) < a);
}
void o_geq16(vcpu_t* cpu)
{
    word a = spop16(&cpu->ds);
    spush(&cpu->ds, spop16(&cpu->ds) >= a);
}
void o_leq16(vcpu_t* cpu)
{
    word a = spop16(&cpu->ds);
    spush(&cpu->ds, spop16(&cpu->ds) <= a);
}

void o_inc16(vcpu_t* cpu)        { (*LITEM(cpu->ds))++; }
void o_dec16(vcpu_t* cpu)        { (*LITEM(cpu->ds))--; }
void o_rnc16(vcpu_t* cpu)        { cpu->ram[spop16(&cpu->ds)]++; }
void o_rec16(vcpu_t* cpu)        { cpu->ram[spop16(&cpu->ds)]--; }

void o_adl16(vcpu_t* cpu)
{
    // spaghet code
    word* ptr = (word*)(cpu->ram + cpu->ar);

    spush16(&cpu->ds, *ptr);
}
void o_ads16(vcpu_t* cpu)
{
    word a    = spop16(&cpu->ds);
    word* ptr = (word*)(cpu->ram + cpu->ar);
    
    *ptr = a;
}

// Signed Operations 16-bit
// Did I ever told you I sucked at programming? No? Well now you know it.
void o_sadd16(vcpu_t* cpu)
    { spush16(&cpu->ds, (sword)spop16(&cpu->ds) + (sword)spop16(&cpu->ds)); }
void o_ssub16(vcpu_t* cpu)
{
    sword a = (sword)spop16(&cpu->ds);
    spush16(&cpu->ds, (sword)spop16(&cpu->ds) - a);
}
void o_smul16(vcpu_t* cpu)
{
    spush16(&cpu->ds, (sword)spop16(&cpu->ds) * (sword)spop16(&cpu->ds));
}
void o_sdiv16(vcpu_t* cpu)
{
    sword a = (sword)spop16(&cpu->ds);
    spush16(&cpu->ds, (sword)spop16(&cpu->ds) / a);
}
void o_sgrt16(vcpu_t* cpu)
{
    sword a = (sword)spop16(&cpu->ds);
    spush(&cpu->ds, (sword)spop16(&cpu->ds) > a);
}
void o_slrt16(vcpu_t* cpu)
{
    sword a = (sword)spop16(&cpu->ds);
    spush(&cpu->ds, (sword)spop16(&cpu->ds) < a);
}
void o_sgeq16(vcpu_t* cpu)
{
    sword a = spop16(&cpu->ds);
    spush(&cpu->ds, (sword)spop16(&cpu->ds) >= a);
}
void o_sleq16(vcpu_t* cpu)
{
    sword a = (sword)spop16(&cpu->ds);
    spush(&cpu->ds, (sword)spop16(&cpu->ds) <= a);
}


void interrupt_call(vcpu_t*, byte);
// This is a special opcode.
void o_uni(vcpu_t* cpu)
{
    interrupt_call(cpu, 1);
}

vcpu_opcode_t opcodes[] = {
   o_halt, o_uni, o_int, o_in, o_out,
    o_rot, o_swp, o_dup, o_over, o_drop,
    o_add, o_sub, o_mul, o_div, o_mod,
    o_and, o_or,  o_not, o_xor, o_sr,  o_sl, o_inv,
    o_ldb, o_stb, o_adl, o_ads,
    o_equ, o_neq, o_grt, o_lrt, o_geq, o_leq,
    o_jmp, o_jmz, o_cal, o_caz, o_ret, o_rez,
    o_inc, o_dec, o_rnc, o_rec,
    o_adr, o_adw, o_adi,

    // Signed Operations
    o_sadd, o_ssub, o_smul, o_sdiv,
    o_sgrt, o_slrt, o_sgeq, o_sleq
};
vcpu_opcode_t opcodes16[] = {
    NULL, NULL, NULL, NULL, NULL,
    o_rot16, o_swp16, o_dup16, o_over16, o_drop16,
    o_add16, o_sub16, o_mul16, o_div16, o_mod16,
    o_and16, o_or16,  o_not16, o_xor16, o_sr16, o_sl16, NULL,
    o_ldw,   o_stw,   o_adl16, o_ads16,
    o_equ16, o_neq16, o_grt16, o_lrt16, o_geq16, o_leq16,
    NULL, NULL, NULL, NULL, NULL, NULL,
    o_inc16, o_dec16, o_rnc16, o_rec16,
    NULL, NULL, NULL,

    // Signed Operations
    o_sadd16, o_ssub16, o_smul16, o_sdiv16,
    o_sgrt16, o_slrt16, o_sgeq16, o_sleq16
};

// Interrupt call
void interrupt_call(vcpu_t* cpu, byte id)
{
    word* table = (word*)(cpu->ram + cpu->ip);
    word  addr  = table[id];
    
    if (addr)
    {
        spush16(&cpu->ds, addr);
        o_cal(cpu);
    }
    else
        // If it points to 0x000, halt the CPU.
        cpu->flags |= _VICSM8_HF;
}

// Init Stack
void init_stack(vcpu_stack_t* s)
{
    s->ptr = 0;
    for (int i = 0; i < _VICSM8_STACKSZ; ++i)
        s->s[i] = 0;
}
// Init CPU
void init_cpu(vcpu_t* cpu)
{
    for (size_t i = 0; i < _VICSM8_MEMORY; ++i)
        cpu->ram[i] = 0;
    for (size_t i = 0; i < 256; ++i)
        cpu->ports[i].enabled = 0;

    cpu->pc    = 0x0000;
    cpu->ip    = 0xf300;
    cpu->flags = 0;

    init_stack(&cpu->ds);
    init_stack(&cpu->rs);
}

// Run an opcode
void run_opcode(vcpu_t* cpu, byte c)
{
    byte bit16  = (c & 0x80) == 0x80;
    byte opcode = c & 0x7f;

    if (bit16 && opcodes16[opcode])
        opcodes16[opcode](cpu);
    else
        opcodes[opcode](cpu);
}

// Execute a CPU opcode then increase the PC
void execute(vcpu_t* cpu)
{
    byte c = cpu->ram[cpu->pc];
    if (c & 0x40)
    {
        o_num(cpu, c & 0x3f);
    }
    else if ((c & 0x7f) >= (sizeof(opcodes)/sizeof(opcodes[0])))
        interrupt_call(cpu, 1);
    else if (c < sizeof(opcodes))
        run_opcode(cpu, c);
    
    ++(cpu->pc);
}
