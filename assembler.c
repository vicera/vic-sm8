/*
 * VIC-SM8 by h34ting4ppliance
 *
 * assembler.c
 *
 * VIC-SM8 bytecode assembler
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

/*
 * Definitions
 */

// Unsigned
typedef uint8_t  byte;
typedef uint16_t word;
// Signed
typedef int8_t   sbyte;
typedef int16_t  sword; // <>--[]======>

// Define
#define ROM_MAXSIZE     32768
#define LBLSIZE         32
#define MAXLABELS       256

// Little hack because strcmp sucks
#define scmp(x, y)      (strcmp(x, y) == 0)

// Type def.
typedef struct
{
    char    name[LBLSIZE];
    word    value;
} asm_label_t;

typedef struct
{
    int     size;
    byte    content[ROM_MAXSIZE];
} asm_prgm_t;

typedef struct
{
    size_t      ptr;
    asm_label_t labels[MAXLABELS];
} asm_lblstack_t;

// Variables
// Variable address counter
word vcount = 0x8000;
// Program
asm_prgm_t  program;

// Label stack
asm_lblstack_t labels;

// Opcodes
char* opcodes[] = {
   "HALT", "NUM", "INT", "IN", "OUT",
    "ROT", "SWP", "DUP", "OVER", "DROP",
    "ADD", "SUB", "MUL", "DIV", "MOD",
    "AND", "OR",  "NOT", "XOR", "SR",  "SL", "INV",
    "LD",  "ST",  "ADL", "ADS",
    "EQU", "NEQ", "GRT", "LRT", "GEQ", "LEQ",
    "JMP", "JMZ", "CAL", "CAZ", "RET", "REZ",
    "INC", "DEC", "RNC", "REC",
    "ADR", "ADW", "ADI",

    "SADD", "SSUB", "SMUL", "SDIV",
    "SGRT", "SLRT", "SGEQ", "SLEQ"
};

/*
 * Functions
 */

static int error(const char* message, const char* w)
{
    if (w)
        fprintf(stderr, "Error at '%s'\n", w);
    fprintf(stderr, "%s\n", message);
    exit(1);
}
static int die(char* message)
{
    perror(message);
    exit(1);
}

// TODO: Redo ALL THIS PART OF THE CODE. IT'S FUCKED UP.

byte find_opcode(const char* opcode)
{
    for (size_t i = 0; i < (sizeof(opcodes)/sizeof(opcodes[0])); ++i)
    {
        if (scmp(opcode, opcodes[i]))
            return i;
    }
    return 0;
}
int ishex(const char* s)
{
    for (int i = 0; s[i]; ++i)
    {
        if (!((s[i] >= '0' && s[i] <= '9') || 
            (s[i] >= 'a' && s[i] <= 'f') ||
            (s[i] >= 'A' && s[i] <= 'F')))
            return 0;
    }
    return 1;
}
void append_opcode(byte opcode)
{
    program.content[program.size++] = opcode;
}
void append_word(word w)
{
    append_opcode(w >> 8);
    append_opcode(w);
}

void append_nums(const char* numbers)
{
    char curnum[3] = {0, 0, 0};
    int  nlen = strlen(numbers);
    
    if (nlen % 2 == 1 || nlen > 512)
        error("Number length must be pair and under 512.", numbers);
    
    for (byte i = 0; i < (nlen/2); ++i)
    {
        curnum[0] = numbers[0]; curnum[1] = numbers[1];
        numbers += 2;

        append_opcode(strtol(curnum, NULL, 16));
    }
}

// Fuck this I am just gonna write spaghetti code.
// it's 12 am and I just want this assembler to be done rn.
// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
void append_value(word value)
{
    append_opcode(0x42);
    append_opcode(value >> 8);
    append_opcode(value);
}

void append_numop(const char* numbers)
{
    append_opcode(0x40 | (byte)strlen(numbers) / 2);
    append_nums(numbers);
}

void append_string(const char* s)
{
    while (*s)
        append_opcode(*(s++));
}

void append_label(const char* name, word value)
{
    printf("Adding %04x to %s at %02lx\n", value, name, labels.ptr);

    strcpy(labels.labels[labels.ptr].name, name);
    labels.labels[labels.ptr].value = value;
    
    labels.ptr++;
}

word find_label(const char* name)
{
    for (size_t i = 0; i < labels.ptr; ++i)
    {
        if (scmp(labels.labels[i].name, name))
            return labels.labels[i].value;
    }
    
    error("This label does not exist.", name);
    return 0;
}

void append_constant(FILE* f, const char* name)
{
    char num[64];
    if (fscanf(f, "%s", num) != 1)
        error("scanf failed.", name - 1);
    if (strlen(num) != 4)
        error("Number must be a 16-bit hexadecimal number", name - 1);

    append_label(name, strtol(num, NULL, 16));
}

void do_comment(FILE* f)
{
    char w[64];
    while (fscanf(f, "%s", w) == 1)
    {
        if (scmp(w, ")"))
            return;
    }

    error("Reached EOF. Expected '}'", NULL);
}

void stage1(FILE* f)
{
    word addr = 0;
    char w[64];
    byte opc;

    while (fscanf(f, "%s", w) == 1)
    {
        if ((opc=find_opcode(w)) || scmp(w, "HALT"))
            addr += 1;
        else if (w[0] == '+' && (opc=find_opcode(w+1)))
            addr += 1;
        else if (ishex(w))
            addr += strlen(w) / 2 + 1;
        else if (w[0] == '|')
            addr = strtol(w + 1, NULL, 16);

        // Label dictionary shit...
        else if (w[0] == '$')
            append_label(w + 1, vcount++);
        else if (w[0] == '#')
            append_constant(f, w + 1);
        else if (w[0] == ':')
            append_label(w + 1, addr);

        else if (w[0] == ',')
            addr += strlen(w + 1) / 2;
        else if (w[0] == '.')
            addr += 3;
        else if (w[0] == ';')
            addr += 2;
        else if (w[0] == '"')
            addr += strlen(w + 1);
        else if (scmp(w, "("))
            do_comment(f);
        else
            error("Invalid instruction", w);
    }
    if (addr >= ROM_MAXSIZE)
        error("ROM Size is above the limit.", NULL);
    rewind(f);
}

void stage2(FILE* f)
{
    char w[64];
    byte opc;

    while (fscanf(f, "%s", w) == 1)
    {
        if ((opc=find_opcode(w)) || scmp(w, "HALT"))
            append_opcode(opc);
        else if (w[0] == '+' && (opc=find_opcode(w+1)))
            append_opcode(0x80 + opc);
        else if (ishex(w))
            append_numop(w);
        else if (scmp(w, "{"))
            do_comment(f);
        
        else if (w[0] == '|') continue;
        else if (w[0] == '$') continue;
        else if (w[0] == '#') fscanf(f, "%s", w);
        else if (w[0] == ':') continue;
        
        else if (w[0] == ',' && ishex(w + 1))
            append_nums(w + 1);
        else if (w[0] == '.')
            append_value(find_label(w + 1));
        else if (w[0] == ';')
            append_word(find_label(w + 1));
        else if (w[0] == '"')
            append_string(w + 1);
        else if (scmp(w, "("))
            do_comment(f);
        else
            error("Invalid instruction", w);
    }
}

/*
 * Main function
 */

int main(int argc, char **argv)
{
    if (argc != 2 && argc != 3)
        error("Usage: sm8asm IN [OUT]", NULL);
    
    const char* outname = argc == 2 ? "out.rom" : argv[2];

    FILE* fin  = fopen(argv[1], "r");
    if (!fin)
        die("sm8asm: fopen IN");
    
    program.size = labels.ptr = 0;

    // Do the magic :O
    stage1(fin);
    stage2(fin);

    FILE* fout = fopen(outname, "w");
    if (!fout)
        die("sm8asm: fopen OUT");
    
    fwrite(program.content, sizeof(byte), program.size, fout);
    fclose(fout);
    fclose(fin);

    return 0;
}
