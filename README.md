<h1 align=center>VIC-SM8</h1>
<p align=center>8-bit stack machine CPU</p>
<p></p>

### [Documentation here](https://wiki.cutebunni.es/prog/vicera/vic-sm8.html)

The VIC-SM8 is the new upcoming CPU for the VICERA fantasy console. It features
a stack-based instruction set and I/O ports. The way this CPU works is
completely different from the original VIC-8P. I/O was memory-mapped, now it's
not.

The VIC-SM8 will also be made to be easier to embed in other applications than
the VICERA itself. This is also another reason why the CPU has been moved to
another repository.

### Example machine's I/O ports.

In case you want to mess around with the example machine, here are the few I/O
ports implemented in. They are here for development and testing purposes.

 - Port 1 is TTY. IN command will read a byte from stdin and push in the stack
   and the OUT command will pop a byte from the stack and write it to stdout
 - Port 2 is STACKTRACE. OUT command will print to stdout the content of the
   data stack.
 - Port 8 is HELLOWORLD. OUT command will print "Hello, World!" to stdout.

### Building

If you want to embed the VM into your application. You might want to move cpu.c
and cpu.h in your project and build your project with it.
I haven't made a `libsm8` for now, however it might come soon, the project is
just in a testing phase for now.

    # Build the shared library
    make
    # Install the shared library
    # There is a INSTALL_DIR macro in the Makefile that you can change.
    sudo make install
    # Build the example machine
    make example
    # Build the assembler
    make assembler

### Contribution

Pull Requests are welcome! The code structure must just match the one I am using.

### Licensing

This project is licensed under the MIT License.

![logo](logo.png)
