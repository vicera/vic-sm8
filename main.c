/*
 * VIC-SM8 CPU by matthilde
 *
 * main.c
 *
 * This is an example front-end that uses the CPU. This main.c and the makefile
 * that comes with is here for documentation purposes to help you how to roll
 * your own VIC-SM8 machine.
 *
 * The extensibility and flexibility has been much more improved compared to the
 * VIC-8P. I suggest you to compare this with the Brainfuck interpreter written
 * in VIC-8P assembly as it uses a custom front-end.
 * 
 * https://git.cutebunni.es/vcollection/vic8p-brainfuck
 */
#include <stdio.h>
#include <stdlib.h>
#include "cpu.h"

#define log(msg) printf("[#] " msg "\n")

void io_handler_test(vcpu_t* cpu, byte _)
{
    printf("Hello, World!\n");
}

void io_stacktrace(vcpu_t* cpu, byte _)
{
    for (size_t i = 0; i < cpu->ds.ptr; ++i)
        printf("%02x ", cpu->ds.s[i]);
    puts("");
}

// TTY device
void io_tty_device_out(vcpu_t* cpu, byte _)
{
    putchar((char)spop(&cpu->ds));
}
void io_tty_device_in(vcpu_t* cpu, byte _)
{
    spush(&cpu->ds, (byte)getchar());
}

vcpu_ioport_t ttydev = {
    1, "TTY", NULL,
    io_tty_device_in,
    io_tty_device_out,
    NULL
};

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        printf("Usage: vicsm8 ROMFILE\n");
        return 1;
    }

    // Init CPU
    vcpu_t computer;
    init_cpu(&computer);

    FILE* f;
    if ((f=fopen(argv[1], "r")) == NULL)
    {
        perror("fopen");
        return 1;
    }
    int c;
    for (int i = 0; (c=fgetc(f)) != EOF; ++i)
        computer.ram[i] = c;
    fclose(f);

    // IO Devices
    computer.ports[8].enabled = 1;
    computer.ports[8].out = io_handler_test;
    computer.ports[2].enabled = 1;
    computer.ports[2].out = io_stacktrace;
    computer.ports[1] = ttydev;

    while (!(computer.flags & _VICSM8_HF))
        execute(&computer);

    io_stacktrace(&computer, 0);

    return 0;
}
