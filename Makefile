# CPU files
FILES = cpu.o
# Assembler files
ASM_FILES = assembler.c
# Example machine files
EM_FILES = main.c
# Output file names
EXECNAME = vicsm8
LIBNAME  = libsm8.so

# All files
ALL_FILES = $(FILES) $(ASM_FILES) $(EM_FILES) $(EXECNAME) $(LIBNAME)

CFLAGS = -c -fpic
LFLAGS = -Wall -Wextra -std=c99

CC = gcc
LD = gcc

.PHONY: clean install

#### CPU ####

$(LIBNAME): $(FILES)
	$(LD) $(LFLAGS) $(FILES) -Wl,-soname,$(LIBNAME) -shared -o $(LIBNAME)

%.o: %.c
	$(CC) $< $(CFLAGS) -o $@

#### ASSEMBLER ####

assembler: assembler.c
	$(CC) assembler.c $(LFLAGS) -o sm8asm

#### EXAMPLE MACHINE ####

ELFLAGS = -L.

example: $(EM_FILES)
	$(CC) $(EM_FILES) $(ELFLAGS) -lsm8 -o $(EXECNAME)

#### .PHONY ####

INSTALL_DIR = /usr/local

install:
	cp $(LIBNAME) $(INSTALL_DIR)/lib/$(LIBNAME)
	chmod 755 $(INSTALL_DIR)/lib/$(LIBNAME)
	cp cpu.h $(INSTALL_DIR)/include/vicsm8.h
	ldconfig

clean:
	rm -f $(EXECNAME) $(LIBNAME) $(FILES)
